# pin_guessable
This is a little software collection written in Python2 and 3 to show how bruteforce attacks work.
It consists of two features:
One script tells you, how many possibilities there are with x digits of pin.
The other two demonstrates simple bruteforce attacks:
One one PINs, the other one on passwords.

Possibly following features:
* Adding anti-crash-code

	Warning: This software is only ment to be used for educational and demonstration purposes. You are not allowed to use it to attack other people
